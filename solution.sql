CREATE DATABASE blog_db;

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(300) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
	author_id INT NOT NULL,
	title VARCHAR(500),
	content VARCHAR(5000),
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_posts_author_id
		FOREIGN KEY (author_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE post_comments(
	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	content VARCHAR(5000),
	datetime_commented DATETIME NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_post_comments_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_comments_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE post_likes(
	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	datetime_liked DATETIME NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_post_likes_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_likes_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

INSERT INTO users(email,password)
VALUES
("johnsmith@gmail.com","passwordA"),
("juandelacruz@gmail.com","passwordB"),
("janesmith@gmail.com","passwordC",),
("mariadelacruz@gmail.com","passwordD"),
("johndoe@gmail.com","passwordE");

UPDATE users SET datetime_created = 20210101010000;

INSERT INTO posts(author_id,title,content,datetime_posted)
VALUES
(1,"First Code","Hello World!",20210102010000),
(1,"Second Code","Hello Earth!",20210102020000),
(2,"Third Code","Welcome to Mars!",20210102030000),
(4,"Fourth Code","Bye bye solar system!",20210102040000);

SELECT title,content FROM posts WHERE author_id = 1;

SELECT email,datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

DELETE FROM users WHERE email = "johndoe@gmail.com";







